package com.schoolexercise.weatherapi;

import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class Functions {

    public static void toast(Context mContext , String msg){
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }

    public static void setData(String key , String value){
        Hawk.put(key , value) ;
    }
    public static String getData(String key){
        return Hawk.get(key) ;
    }
}
