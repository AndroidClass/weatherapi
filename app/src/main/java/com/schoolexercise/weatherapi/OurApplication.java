package com.schoolexercise.weatherapi;

import android.app.Application;

import com.orhanobut.hawk.Hawk;
import com.orm.SugarApp;

public class OurApplication extends SugarApp {

    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
