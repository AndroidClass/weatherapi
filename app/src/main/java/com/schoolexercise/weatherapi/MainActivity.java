package com.schoolexercise.weatherapi;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.IpSecManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;


import com.schoolexercise.weatherapi.models.Forecast;
import com.schoolexercise.weatherapi.models.Yahoo;
import com.schoolexercise.weatherapi.models.citybyip.City;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;



public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText city;
    TextView textCityName, textCentralWeather, textCentralTemperature, textCentralDay, textCentralDate;
    ImageView imageCentral;
    ListView forecast;
    Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        city = findViewById(R.id.city);

        textCityName = findViewById(R.id.textCityName);
        textCentralWeather = findViewById(R.id.textCentralWeather);
        textCentralTemperature = findViewById(R.id.textCentralTemperature);
        textCentralDay = findViewById(R.id.textCentralDay);
        textCentralDate = findViewById(R.id.textCentralDate);
        imageCentral = findViewById(R.id.imageCentral);


        findViewById(R.id.show).setOnClickListener(this);

        forecast = findViewById(R.id.forecast);


        getLocationByIp();
    }

    @Override
    public void onClick(View view) {

        String cityName = city.getText().toString();

        if(cityName.isEmpty()){cityName="tehran";}

        getData(cityName);

    }

    void getData(String city) {
if (city.isEmpty()){city="tehran";}
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" + city + "%2C%20ir%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Functions.toast(mContext, "Failed");

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndShowYahooWeather(responseString);
            }
        });
    }

void getLocationByIp(){

    String url ="https://geoipify.whoisxmlapi.com/api/v1?apiKey=at_QbeIeqyp2w4Vw9pNZXZXLJ39BFO4w";
    //String url = "https://api.ipdata.co/?api-key=19344ff6b36b81dbe0633a048669b267a089fe9aa98f1bb6bef3df2b";
    AsyncHttpClient client = new AsyncHttpClient();
    client.get(url, new TextHttpResponseHandler() {
        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            Functions.toast(mContext, "Failed");
            getData("tehran");
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
            parseAndShowLocationByIp(responseString);
        }
    });

}

void parseAndShowLocationByIp(String data){

    Gson gson = new Gson();

    City city = gson.fromJson(data, City.class);

      String cityName = city.getLocation().getCity();

      if(cityName.isEmpty()){cityName="tehran";}

    getData(cityName);
}


void parseAndShowYahooWeather(String data) {
        Gson gson = new Gson();

        Yahoo yahoo = gson.fromJson(data, Yahoo.class);

        String tempValue =
                yahoo.getQuery().getResults()
                        .getChannel().getItem()
                        .getCondition().getTemp();

        textCentralTemperature.setText(fToC(tempValue) + " °C ");

        String statusValue =
                yahoo.getQuery().getResults()
                        .getChannel().getItem()
                        .getCondition().getText();

        textCentralWeather.setText(statusValue);


        String cityValue =
                yahoo.getQuery().getResults()
                        .getChannel().getLocation().getCity();

        String countryValue =
                yahoo.getQuery().getResults()
                        .getChannel().getLocation().getCountry();

        textCityName.setText(cityValue + " / " + countryValue);

        String dayValue =
                yahoo.getQuery().getResults().getChannel().getWind().getSpeed();

        textCentralDay.setText("Wind Speed "+dayValue+"mph");

        String dateValue =
                yahoo.getQuery().getResults().getChannel().getAtmosphere().getHumidity();

        textCentralDate.setText("Humidity  %"+dateValue);


    String imgAddress = "https://findicons.com/files/icons/2607/tick_weather_icons/128/cloudy4.png" ;
    if( statusValue.equalsIgnoreCase("Partly Cloudy")){
        imgAddress = "https://findicons.com/files/icons/478/weather/128/sunny_interval.png";
    }
    if( statusValue.equalsIgnoreCase("Mostly Sunny")){
        imgAddress="https://findicons.com/files/icons/1786/oxygen_refit/128/weather_clear.png";
    }
//https://findicons.com/files/icons/1786/oxygen_refit/128/weather_snow.png
    //https://findicons.com/files/icons/1786/oxygen_refit/128/weather_clear.png
    //https://findicons.com/files/icons/1786/oxygen_refit/128/weather_showers.png


    Glide.with(mContext)
            .load(imgAddress).into(imageCentral) ;

    List<Forecast> forecastsList =
            yahoo.getQuery().getResults()
                    .getChannel().getItem().getForecast() ;

    ForecastListAdapter adapter =
            new ForecastListAdapter(mContext , forecastsList);

    forecast.setAdapter(adapter);
    }

    String fToC(String tempValueParam) {

        int result;
        result = Integer.parseInt(tempValueParam);
        result = (int) ((result - 32) / 1.8);

        return result + "";
    }

}
