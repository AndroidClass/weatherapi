
package com.schoolexercise.weatherapi.models.citybyip;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class City {

    @SerializedName("ip")
    @Expose
    private String ip;
    @SerializedName("location")
    @Expose
    private Location location;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
