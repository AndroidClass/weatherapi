package com.schoolexercise.weatherapi;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.schoolexercise.weatherapi.models.Forecast;

import java.util.List;

public class ForecastListAdapter extends BaseAdapter {

    Context mContext;
    List<Forecast> forecast;

    public ForecastListAdapter(Context mContext, List<Forecast> forecast) {
        this.mContext = mContext;
        this.forecast = forecast;
    }


    @Override
    public int getCount() {
        return forecast.size();
    }

    @Override
    public Object getItem(int position) {
        return forecast.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View v = LayoutInflater.from(mContext)
                .inflate(R.layout.forecast_list_item,viewGroup,false);

        TextView day = v.findViewById(R.id.day) ;
        TextView high = v.findViewById(R.id.high) ;
        TextView low = v.findViewById(R.id.low) ;
        TextView text = v.findViewById(R.id.text) ;
        ImageView logo = v.findViewById(R.id.logo) ;

        day.setText(forecast.get(position).getDay());
        high.setText(fToC(forecast.get(position).getHigh())+" °C ");
        low.setText(fToC(forecast.get(position).getLow())+" °C ");
        text.setText(forecast.get(position).getText());

        String imgAddress = "https://findicons.com/files/icons/2607/tick_weather_icons/128/cloudy4.png" ;
        if( forecast.get(position).getText().equalsIgnoreCase("Partly Cloudy")){
            imgAddress = "https://findicons.com/files/icons/479/weather/128/cloudy_nighttime.png";
        }
        if( forecast.get(position).getText().equalsIgnoreCase("Mostly Sunny")){
            imgAddress="https://findicons.com/files/icons/2606/the_weather/128/moon_cloudy.png";
        }

        Glide.with(mContext)
                .load(imgAddress).into(logo) ;

        return v;
    }

    String fToC(String tempValueParam) {

        int result;
        result = Integer.parseInt(tempValueParam);
        result = (int) ((result - 32) / 1.8);

        return result + "";
    }
}
