package com.schoolexercise.weatherapi.db;

import com.orm.SugarRecord;

public class WeatherEntity extends SugarRecord<WeatherEntity> {

    String city,weatherText,temperature,windSpeed,humidity;

    WeatherForecast weatherForecast;

    public WeatherEntity() {
    }

    public WeatherEntity(String city, String weatherText, String temperature, String windSpeed, String humidity, WeatherForecast weatherForecast) {
        this.city = city;
        this.weatherText = weatherText;
        this.temperature = temperature;
        this.windSpeed = windSpeed;
        this.humidity = humidity;
        this.weatherForecast = weatherForecast;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public void setWeatherText(String weatherText) {
        this.weatherText = weatherText;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public WeatherForecast getWeatherForecast() {
        return weatherForecast;
    }

    public void setWeatherForecast(WeatherForecast weatherForecast) {
        this.weatherForecast = weatherForecast;
    }
}
